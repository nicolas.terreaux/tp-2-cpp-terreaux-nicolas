#include "Circle.hpp"


Circle::Circle(Point pt, int r) : Shape(), r_(r) {
    std::vector<Point> pts;
    r = r / 2;
    // Consider a rectangle of size N*N
    int N = 2 * r + 1;

    int x, y; // Coordinates inside the rectangle

    // Draw a square of size N*N.
    for (int i = 0; i < N; i++) {
        for (int j = 0; j < N; j++) {
            // Start from the left most corner point
            x = i - r;
            y = j - r;
            // If this point is inside the circle, print it
            if (x * x + y * y <= r * r + 1) {
                pts.push_back(Point(pt.getX() + i, pt.getY() + j));
            }
        }
    }
    points_ = pts;
}

Circle::~Circle() {

}

//Copy constructor
Circle::Circle(const Circle &circle) : r_(circle.r_) {

}

// Affectation operator
Circle &Circle::operator=(const Circle &circle) {
    r_ = circle.r_;
    return *this;
}

