#include <vector>
#include "Ellipse.hpp"

using namespace std;

Ellipse::Ellipse(Point pt, int width, int height) : witdth_(width), height_(height) {
    std::vector<Point> pts;

    double r1 = (width - 1) / 2;
    double r2 = (height - 1) / 2;
    for (int i = 0; i < width; i++) {
        for (int j = 0; j < height; j++) {
            double delta_i = i - r1;
            double delta_j = j - r2;
            double elt_i = (delta_i * delta_i) / (r1 * r1);
            double elt_j = (delta_j * delta_j) / (r2 * r2);
            double elt = elt_i + elt_j;
            bool isInEllipse = elt <= 1;
            if (isInEllipse) {
                pts.push_back(Point(pt.getX() + i, pt.getY() + j));
            }
        }
    }
    points_ = pts;
}

Ellipse::~Ellipse() {

}

//Copy constructor
Ellipse::Ellipse(const Ellipse &ellipse) : witdth_(ellipse.witdth_), height_(ellipse.height_) {

}

// Affectation operator
Ellipse &Ellipse::operator=(const Ellipse &ellipse) {
    witdth_ = ellipse.height_;
    height_ = ellipse.height_;
    return *this;
}

