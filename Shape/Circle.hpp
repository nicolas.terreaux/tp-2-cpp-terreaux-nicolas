// Circle.h

#ifndef CIRCLE_H
#define CIRCLE_H

#include <iostream>
#include "../Point/Point.hpp"
#include "Shape.hpp"


class Circle : public Shape {

public:
    Circle(Point pt, int r);

    ~Circle();

    Circle(const Circle &circle);

    Circle &operator=(const Circle &circle);


private:
    int r_;
};

#endif /* CIRCLE_H */
