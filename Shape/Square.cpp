#include "Square.hpp"


Square::Square(Point pt, int size) : Rectangle(pt, size, size), size_(size) {

    std::vector<Point> pts;

    for (int h = 0; h < size; h++) {
        for (int w = 0; w < size; w++) {
            pts.push_back(Point(pt.getX() + w, pt.getY() + h));
        }
    }
    points_ = pts;
}

Square::~Square() {

}

//Copy constructor
Square::Square(Point pt, int height, int width, const Square &square) : Rectangle(pt, height, width),
                                                                        size_(square.size_) {

}

// Affectation operator
Square &Square::operator=(const Square &square) {
    size_ = square.size_;
    return *this;
}

