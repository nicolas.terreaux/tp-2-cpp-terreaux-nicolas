#include "Rectangle.hpp"
#include "vector"


Rectangle::Rectangle(Point pt, int height, int width) : Shape(), height_(height), witdth_(width) {

    std::vector<Point> pts;

    for (int h = 0; h < height; h++) {
        for (int w = 0; w < width; w++) {
            pts.push_back(Point(pt.getX() + w, pt.getY() + h));
        }
    }
    points_ = pts;
}

Rectangle::~Rectangle() {

}

// Copy constructor
Rectangle::Rectangle(const Rectangle &rectangle) : height_(rectangle.height_), witdth_(rectangle.witdth_) {

}

// Affectation operator
Rectangle &Rectangle::operator=(const Rectangle &rectangle) {
    height_ = rectangle.height_;
    witdth_ = rectangle.witdth_;
    return *this;
}

