// Rectangle.h

#ifndef RECTANGLE_H
#define RECTANGLE_H

#include <iostream>
#include "../Point/Point.hpp"
#include "Shape.hpp"


class Rectangle : public Shape {

public:
    Rectangle(Point pt, int height, int width);

    ~Rectangle();

    Rectangle(const Rectangle &rectangle);

    Rectangle &operator=(const Rectangle &rectangle);


private:
    int height_;
    int witdth_;
};

#endif /* RECTANGLE_H */
