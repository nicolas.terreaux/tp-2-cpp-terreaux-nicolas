// Ellipse.h

#ifndef ELLIPSE_H
#define ELLIPSE_H

#include <iostream>
#include "../Point/Point.hpp"
#include "Shape.hpp"


class Ellipse : public Shape {

public:
    Ellipse(Point pt, int width, int height);

    ~Ellipse();

    Ellipse(const Ellipse &ellipse);

    Ellipse &operator=(const Ellipse &ellipse);


private:
    int witdth_;
    int height_;
};

#endif /* ELLIPSE_H */
