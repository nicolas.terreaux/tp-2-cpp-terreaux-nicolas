// Shape.h

#ifndef SHAPE_H
#define SHAPE_H

#include <iostream>
#include <vector>
#include "../Point/Point.hpp"


class Shape {

public:
    Shape(const std::vector<Point> &points);

    ~Shape();

    Shape &operator=(const Shape &shape);

    std::vector<Point> getPoints();


protected:
    Shape();

    std::vector<Point> points_;

};

#endif /* SHAPE_H */
