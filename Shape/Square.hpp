// Square.h

#ifndef SQUARE_H
#define SQUARE_H

#include <iostream>
#include "../Point/Point.hpp"
#include "Rectangle.hpp"


class Square : public Rectangle {

public:
    Square(Point pt, int size);

    ~Square();

    Square &operator=(const Square &square);

    Square(Point pt, int height, int width, const Square &square);

private:
    int size_;
};

#endif /* SQUARE_H */
