#include "Shape.hpp"


Shape::Shape(const std::vector<Point> &points) : points_(points) {}

Shape::~Shape() {

}

Shape::Shape() {}

std::vector<Point> Shape::getPoints() {
    return points_;
}

// Affectation operator
Shape &Shape::operator=(const Shape &shape) {
    points_ = shape.points_;
    return *this;
}

