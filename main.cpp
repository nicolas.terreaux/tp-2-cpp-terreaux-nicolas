#include <iostream>
#include <ctime>
#include "Picture/Picture.hpp"
#include "Shape/Rectangle.hpp"
#include "Shape/Square.hpp"
#include "Shape/Circle.hpp"
#include "Shape/Ellipse.hpp"

constexpr int MIN = 50;
constexpr int MAX = 200;
constexpr int MIN_SHAPE = 1;
constexpr int MAX_SHAPE = 4;
constexpr int MIN_XY = 0;
constexpr int MIN_SIZE = 25;
constexpr int MAX_SIZE = 150;
constexpr int SQUARE = 1;
constexpr int RECTANGLE = 2;
constexpr int CIRCLE = 3;
constexpr int ELLIPSE = 4;
constexpr int MAX_X = 1000;
constexpr int MAX_Y = 1000;

int main(int argc, char **argv) {
    std::string fileName;

    //Check if there is an output file. If there is none, a default one is used
    if (argc <= 1) {
        fileName = "./output/file.txt";
    } else if (argc == 2) {
        fileName = argv[1];
    } else {
        std::cerr << "Please give the output file" << std::endl;
        std::cerr << "Example: output.txt" << std::endl;
        exit(EXIT_FAILURE);
    }

    //Generate all shapes randomly (type, number and size)
    srand(time(NULL));
    Picture pc(MAX_Y, MAX_X);
    int nbShapes = rand() % (MAX - MIN + 1) + MIN;
    for (int i = 0; i < nbShapes; i++) {
        int typeShape = rand() % (MAX_SHAPE - MIN_SHAPE + 1) + MIN_SHAPE;
        int size1 = rand() % (MAX_SIZE - MIN_SIZE + 1) + MIN_SIZE;
        int size2 = rand() % (MAX_SIZE - MIN_SIZE + 1) + MIN_SIZE;
        int x = rand() % (MAX_X - MIN_XY + 1) + MIN_XY;
        int y = rand() % (MAX_Y - MIN_XY + 1) + MIN_XY;

        Point pt(x, y);
        switch (typeShape) {
            case SQUARE: {
                Rectangle rec(pt, size1, size2);
                pc.addShape(rec);
                break;
            }
            case RECTANGLE: {
                Square sq(pt, size1);
                pc.addShape(sq);
                break;
            }
            case CIRCLE: {
                Circle cir(pt, size1);
                pc.addShape(cir);
                break;
            }
            case ELLIPSE: {
                Ellipse ell(pt, size1, size2);
                pc.addShape(ell);
                break;
            }
            default:
                break;
        }
    }

    //Draw tha atwork
    pc.draw(fileName);
    return 0;
}
