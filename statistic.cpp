#include <iostream>
#include <fstream>
#include <vector>


using std::cout; using std::cerr;
using std::endl; using std::string;
using std::ifstream; using std::vector;


int main(int argc, char **argv) {
    if (argc < 3) {
        cerr << "Please give the input file and the output file" << endl;
        cerr << "Example: input.txt output.md" << endl;
        exit(EXIT_FAILURE);
    }

    int cWhite = 0;
    int cBlack = 0;
    int nbLine = 0;
    int nbColumn = 0;
    int oldNbCol = -1;
    string inputFile = argv[1];
    string outputFile = argv[2];
    string filename(inputFile);

    vector<char> bytes;
    char byte = 0;

    ifstream input_file(filename);
    if (!input_file.is_open()) {
        cerr << "Could not open the file - '"
             << filename << "'" << endl;
        return EXIT_FAILURE;
    }

    while (input_file.get(byte)) {
        bytes.push_back(byte);
    }

    char wh = ' ';
    char bl = '#';
    cout << "Analysing file " + inputFile << endl;

    for (const auto &i: bytes) {

        if (i == bl) cBlack++;
        if (i == wh) cWhite++;

        if (i == '\n') {
            nbLine++;
            if (oldNbCol < nbColumn)oldNbCol = nbColumn;
            nbColumn = 0;
        } else {
            nbColumn++;
        }
    }

    int totalChar = cBlack + cWhite;

    double perBlack = (double) cBlack / (double) totalChar * 100.0;
    double perWhite = (double) cWhite / (double) totalChar * 100.0;


    std::ofstream out;
    out.open(outputFile);
    out << "# Statistique du fichier" + inputFile << endl;
    out << "" << endl;
    out << "Nb total of char = " + std::to_string(totalChar) << endl;
    out << "" << endl;
    out << "Nb of black = " + std::to_string(cBlack) << endl;
    out << "" << endl;
    out << "Nb of white = " + std::to_string(cWhite) << endl;
    out << "" << endl;
    out << "Percentage of black = " + std::to_string(perBlack) + "%" << endl;
    out << "" << endl;
    out << "Percentage of white = " + std::to_string(perWhite) + "%" << endl;
    out << "" << endl;
    out << "Dimension of the image = " + std::to_string(oldNbCol) + " x " + std::to_string(nbLine) << endl;
    out.close();
    cout << "Statistics done and written to the " + outputFile + " file" << endl;


    return 0;
}
