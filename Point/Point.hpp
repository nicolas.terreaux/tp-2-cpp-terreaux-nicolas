// Point.h

#ifndef POINT_H
#define POINT_H

#include <iostream>


class Point {

public:
    Point();
    Point(int x, int y);
    ~Point();
    Point(const Point &point);

    Point &operator=(const Point &point);


    int getX() const;

    int getY() const;

private:
    int x_;
    int y_;

};

#endif /* POINT_H */
