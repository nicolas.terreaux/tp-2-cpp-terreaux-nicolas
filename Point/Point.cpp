#include "Point.hpp"

Point::Point() {}

Point::Point(int x, int y) : x_(x), y_(y) {}

int Point::getX() const {
    return x_;
}

int Point::getY() const {
    return y_;
}

Point::~Point() {

}

//Copy constructor
Point::Point(const Point &point) : x_(point.x_), y_(point.y_) {

}

// Affectation operator
Point &Point::operator=(const Point &point) {
    x_ = point.x_;
    y_ = point.y_;
    return *this;
}

