// Picture.h

#ifndef PICTURE_H
#define PICTURE_H

#include <iostream>
#include <vector>
#include "../Point/Point.hpp"
#include "../Shape/Shape.hpp"


class Picture {

public:
    Picture(int height = 1000, int length = 1000);

    ~Picture();

    Picture(const Picture &picture);

    Picture &operator=(const Picture &picture);


    void addShape(Shape shape);

    void draw(std::string fileName);

private:
    int height_;
    int length_;
    std::vector<Shape> shapes_;
    std::vector<Point> points_;

};

#endif /* PICTURE_H */
