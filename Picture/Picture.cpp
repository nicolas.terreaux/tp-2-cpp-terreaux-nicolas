#include "Picture.hpp"
#include <iostream>
#include <fstream>

//Constructor
Picture::Picture(int height, int length) : height_(height), length_(length) {

}

//Destructor
Picture::~Picture() {

}

void Picture::addShape(Shape shape) {
    shapes_.push_back(shape);
}

//Method that draws the beautiful artwork in the file passed in parameter
void Picture::draw(std::string fileName) {
    std::vector<std::vector<bool>> res(height_, std::vector<bool>(length_, false));
    //Create the boolean array (res) to know when there is a "#" and when there is nothing
    for (Shape sh: shapes_) {
        for (Point pt: sh.getPoints()) {
            int ptX = pt.getX();
            int ptY = pt.getY();
            //Be sure to be inside the image
            if (ptX < height_ - 1 && ptY < length_ - 1 && ptX >= 0 && ptY >= 0) {
                res[ptX][ptY] = true;
            }
        }
    }

    //Fills the text file with spaces and "#" according to the boolean array
    std::ofstream out;
    out.open(fileName);
    for (int i = 0; i < height_; i++) {
        for (int j = 0; j < length_; j++) {
            if (res[i][j]) {
                out << "#";
            } else {
                out << " ";
            }
        }
        out << "" << std::endl;
    }
    out.close();
}

//Copy constructor
Picture::Picture(const Picture &picture) : height_(picture.height_), length_(picture.length_) {

}

// Affectation operator
Picture &Picture::operator=(const Picture &picture) {
    height_ = picture.height_;
    length_ = picture.length_;
    return *this;
}

