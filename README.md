# TP2

### Exigences
Chaque exigence pas respectée enlève 0.1 de la note du TP.
- [x] Tous les constructeurs doivent utiliser les listes d'initialisation
- [x] Vous devez utiliser l’héritage quand ceci est possible. Il est aussi possible d’ajouter des
classes additionnelles pour compléter la hiérarchie de classes.
- [x] Toutes les classes implémentent le constructeur de copie
- [x] Toutes les classes implémentent l'opérateur d’affection
- [x] Les attributs des classes sont accessibles seulement à eux même ou leurs sous-classes
- [x] Votre repository ne doit pas contenir des fichiers binaires et temporaires (comme le
repertoire cmake-build-debug ou les fichiers configs de l’IDE comme .idea)
- [x] Âpres chaque commit, le code compile sur le CI de gitlab avec -Wall et -Werror comme
options
- [x] Les deux exécutables crées durant ce TP (création d’image et création des opérations pour
recréer l’image) sont exécutées par le CI et les fichiers générées stockées comme artefacts
- [x] Vous travaillez sur un fork du repo original (lien sur cyberlearn) et devez créer un merge
request avant la deadline.

### Optionnel
- [ ] Créez un programme qui dessine quelque chose de plus intéressant que des formes aléatoires
- [ ] Créez un programme qui lit l’image généré et trouve les instructions nécessaires pour
reproduire l’image. Essayez de minimiser le nombre d’instructions necessaires.
Astuce : Commencez avec une version très simple qui utilise un Square par pixel noir
dans l’image
